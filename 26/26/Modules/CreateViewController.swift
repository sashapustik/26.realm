import UIKit
import RealmSwift

class CreateViewController: UIViewController {
    
    private var type: Object.Type
    
    private let tableView = UITableView()
    private lazy var delegate: UITableViewDelegate = {
        let tableViewDelegate = self.delegate
        return tableViewDelegate
    }()
    
    private lazy var dataSource: UITableViewDataSource = {
        let tableViewDataSource = self.dataSource
        return tableViewDataSource
    }()
    
    var object: Object?
    
    //MARK: Init view and provider
    init(textFieldModels: [TextFieldModel], object: Object?, type: Object.Type) {
        let createProvider = CreateProvider(
            textFieldModels: textFieldModels,
            object: object,
            type: type
        )
        self.type = type
        self.object = createProvider.getObject()
        super.init(nibName: nil, bundle: nil)
        
        createProvider.createDelegate = self
        createProvider.setInfoDelegate = self
        
        dataSource = createProvider
        delegate = createProvider
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = delegate
        tableView.dataSource = dataSource
        
        view.backgroundColor = .white
        hiddenKeyboard(to: tableView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard let delegate = delegate as? CreateProvider else { return }
        delegate.reloadData()
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initTableView()
        setTableViewConstraints()
        setAddStudent()
    }
    
    //MARK: Init tableView
    private func initTableView() {
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        tableViewRegisterCells()
        self.view.addSubview(tableView)
    }
    
    //MARK: Register Cells
    private func tableViewRegisterCells() {
        tableView.register(
            CreateTableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.createCell
        )
        tableView.register(
            UITableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.defaultCell
        )
        tableView.register(
            ItemTableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.itemCell
        )
        tableView.register(
            HeaderView.self,
            forHeaderFooterViewReuseIdentifier: Constants.CellId.headerId
        )
    }
    
    //MARK: Table view constraints
    private func setTableViewConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            tableView.topAnchor.constraint(
                equalTo: self.view.topAnchor,
                constant: .zero
            ),
            tableView.trailingAnchor.constraint(
                equalTo: self.view.trailingAnchor,
                constant: -Constants.Constraints.editEdge
            ),
            tableView.leadingAnchor.constraint(
                equalTo: self.view.leadingAnchor,
                constant: Constants.Constraints.editEdge
            ),
            tableView.bottomAnchor.constraint(
                equalTo: self.view.bottomAnchor,
                constant: .zero
            )
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    //MARK: Navigation button
    private func setAddButton() {
        if let course = object as? CourseModel {
            navigationItem.rightBarButtonItems = [
                .init(
                    barButtonSystemItem: .add,
                    target: self,
                    action: #selector(add)
                ),
                .init(
                    title: course.teacher != nil
                     ? Constants.Titles.changeTeacher
                        : Constants.Titles.addTeacher,
                    style: .done,
                    target: self,
                    action: #selector(addTeacher)
                )
            ]
        } else {
            navigationItem.rightBarButtonItem = .init(
                      barButtonSystemItem: .add,
                      target: self,
                      action: #selector(add)
            )
        }
    }
    
    //MARK: Add button for course model
    private func setAddStudent() {
        let button = UIButton()
        button.setTitle(Constants.Titles.add, for: .normal)
        button.addTarget(self, action: #selector(addStudets), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = #colorLiteral(red: 1, green: 0.4769005801, blue: 0.4608249571, alpha: 1)
        
        button.layer.cornerRadius = Constants.ButtonSetup.cornerRadius  
        button.titleLabel?.numberOfLines = Constants.ButtonSetup.numberOfLines
        button.layer.shadowOpacity = Constants.ButtonSetup.shadowOpacity
        button.layer.shadowOffset = .init(
            width: Constants.ButtonSetup.shodowOffset,
            height: Constants.ButtonSetup.shodowOffset
        )
        button.contentEdgeInsets = .init(
            top: .zero,
            left: Constants.ButtonSetup.edgeXY,
            bottom: .zero,
            right: Constants.ButtonSetup.edgeXY
        )
        guard let tabBar = tabBarController?.tabBar else { return }
        let constraints = [
            button.bottomAnchor.constraint(
                equalTo: tableView.bottomAnchor,
                constant: -Constants.ButtonSetup.edgeXY - tabBar.frame.height
            ),
            button.trailingAnchor.constraint(
                equalTo: tableView.trailingAnchor,
                constant: -Constants.ButtonSetup.edgeXY
            ),
            button.widthAnchor.constraint(greaterThanOrEqualToConstant: Constants.ButtonSetup.size),
            button.heightAnchor.constraint(equalToConstant:  Constants.ButtonSetup.size)
        ]
        
       
        if object is CourseModel {
            guard let object = object as? CourseModel else { return }
            let strings = Set(object.getAllData())
            if strings.count != 1 {
                view.addSubview(button)
                NSLayoutConstraint.activate(constraints)
            }
        }
    }

    //MARK: Handlers
    @objc
    private func add() {
        guard
            let realm = try? Realm(),
            let object = object else { return }
        try? realm.write {
            realm.add(object)
        }
        navigationController?.popViewController(animated: true)
    }

    @objc
    private func addTeacher() {
        guard let course = object as? CourseModel else { return }
        let teacherProvider = TeacherProvider()
        let viewController = ViewController(
            delegate: teacherProvider,
            dataSource: teacherProvider
        )
        teacherProvider.setCourse(course: course)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc
    private func addStudets() {
        let studentProvider = StudentProvider()
        let viewController = ViewController(delegate: studentProvider, dataSource: studentProvider)
        guard let course = object as? CourseModel else { return }
        studentProvider.setCourse(course: course)
        setInfo(to: viewController)
    }
}

//MARK: - CreateDelegate
extension CreateViewController: CreateDelegate {
    func addButton(isHidden: Bool) {
        isHidden
            ? navigationItem.rightBarButtonItems = []
            : setAddButton()
    }
    
    func setErrorAlert(messsage: String) {
        self.alert(message: messsage)
    }
    
    func setInfo(with object: Object) -> [String] {
        switch type {
        case is StudentModel.Type:
            guard let student = object as? StudentModel else { return [] }
            return student.getAllData()
        case is TeacherModel.Type:
            guard let teacher = object as? TeacherModel else { return [] }
            return teacher.getAllData()
        case is CourseModel.Type:
            guard let course = object as? CourseModel else { return [] }
            return course.getAllData()
        default:
            return []
        }
    }
    
    func set(data: (String, String), type: Object.Type) {
        let (key, value) = data
        try? Realm().write {
            object?.setValuesForKeys([key: value])
        }
    }
}

//MARK: - SetInfoDelegate
extension CreateViewController: SetInfoDelegate {
    func setInfo(to viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
