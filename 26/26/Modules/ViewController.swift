import UIKit
import RealmSwift

class ViewController: UIViewController {
    
    private var tableView = UITableView()
    
    private lazy var delegate: UITableViewDelegate = {
        return self.delegate
    }()
    
    private lazy var dataSource: UITableViewDataSource = {
        return self.dataSource
    }()
    
    //MARK: Init
    init(delegate: UITableViewDelegate, dataSource: UITableViewDataSource) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
        self.dataSource = dataSource
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        initAddButton()
        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.1129621491, green: 0.6064568162, blue: 0.9659512639, alpha: 1)
        tableView.delegate = delegate
        tableView.dataSource = dataSource
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reloadData()
    }
    
    //MARK: Init tableView
    private func initTableView() {
        view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        fullConstraints(for: tableView, to: view)        
        tableView.register(
            ItemTableViewCell.self,
            forCellReuseIdentifier: Constants.CellId.defaultCell
        )
    }
    
    //MARK: Navigation buttons
    private func initAddButton() {
        if delegate is StudentProvider {
            guard let student = delegate as? StudentProvider else { return }
            if student.courseIsSelected() {
                return
            }
        } else if delegate is TeacherProvider {
            guard let teacher = delegate as? TeacherProvider else { return }
            if teacher.courseIsSelected() {
                return
            }
        }
        navigationItem.rightBarButtonItem = .init(
            barButtonSystemItem: .add,
            target: self,
            action: #selector(add)
        )
    }
    
    //MARK: Reload data for table
    private func reloadData() {
        switch delegate.self {
        case is StudentProvider:
            guard let delegate = delegate as? StudentProvider else { return }
            delegate.reloadData()
        case is CourseProvider:
            guard let delegate = delegate as? CourseProvider else { return }
            delegate.reloadData()
        case is TeacherProvider:
            guard let delegate = delegate as? TeacherProvider else { return }
            delegate.reloadData()
        default:
            break
        }
        tableView.reloadData()
    }
    
    //MARK: Hundlers
    @objc
    private func add() {
        let tags = Constants.TabBarSetup.Tags.self
        switch tabBarController?.selectedIndex {
        case tags.student.rawValue:
            navigationController?.pushViewController(
                CreateViewController(
                    textFieldModels: TextFieldModel.getTextFields(with: tags.student.rawValue),
                    object: nil,
                    type: StudentModel.self
                ),
                animated: true
            )
        case tags.course.rawValue:
            navigationController?.pushViewController(
                CreateViewController(
                    textFieldModels: TextFieldModel.getTextFields(with: tags.course.rawValue),
                    object: nil,
                    type: CourseModel.self
                ),
                animated: true
            )
        case tags.teacher.rawValue:
            navigationController?.pushViewController(
                CreateViewController(
                    textFieldModels: TextFieldModel.getTextFields(with: tags.teacher.rawValue),
                    object: nil,
                    type: TeacherModel.self
                ),
                animated: true
            )
        default:
            break
        }
    }
}

//MARK: - SetInfoDelegate
extension ViewController: SetInfoDelegate {
    func setInfo(to viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
}
