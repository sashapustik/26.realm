import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        window?.rootViewController = setTabBarSetup()
        window?.makeKeyAndVisible()
    }

    private func setTabBarSetup() -> UITabBarController {
        let tabBarController = UITabBarController()
        
        tabBarController.setViewControllers(
            setupViewControllers(),
            animated: true
        )
        let images = [
            UIImage(systemName: Constants.TabBarSetup.Images.student),
            UIImage(systemName: Constants.TabBarSetup.Images.course),
            UIImage(systemName: Constants.TabBarSetup.Images.teacher)
        ]
        let names = [
            Constants.TabBarSetup.Names.student,
            Constants.TabBarSetup.Names.course,
            Constants.TabBarSetup.Names.teacher
        ]
        guard let tabBarItems = tabBarController.tabBar.items else { return UITabBarController() }
        for index in  .zero..<tabBarItems.count {
            tabBarItems[index].image = images[index]
            tabBarItems[index].title = names[index]
        }
        tabBarController.tabBar.tintColor = #colorLiteral(red: 0.9966171384, green: 0.5380721688, blue: 0.4036274552, alpha: 1)
        
        return tabBarController
    }
    
    private func setupViewControllers() -> [UINavigationController] {
        //Providers
        let studentProvider = StudentProvider()
        let courseProvider = CourseProvider()
        let teacherProvider = TeacherProvider()
        
        //Student view controler and her provider
        let studentViewController = ViewController(
            delegate: studentProvider,
            dataSource: studentProvider
        )
        studentProvider.sendDelegate = studentViewController
        
        //Course view controler and her provider
        let courseViewController = ViewController(
            delegate: courseProvider,
            dataSource: courseProvider
        )
        courseProvider.sendDelegate = courseViewController
        
        //Teacher view controler and her provider
        let teacherViewController = ViewController(
            delegate: teacherProvider,
            dataSource: teacherProvider
        )
        teacherProvider.sendDelegate = teacherViewController
        
        let studentNavigationViewController = UINavigationController(rootViewController: studentViewController)
        let teacherNavigationViewController = UINavigationController(rootViewController: teacherViewController)
        let courseNavigationViewController = UINavigationController(rootViewController: courseViewController)
        
        return [
            studentNavigationViewController,
            courseNavigationViewController,
            teacherNavigationViewController
        ]
    }
}
