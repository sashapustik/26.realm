import UIKit

class ItemTableViewCell: UITableViewCell {
    
    private var nameLabel = UILabel()
    private var detailInfoTextLabel = UILabel()
    
    private var stackView = UIStackView()
    private var colorView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let view = UIView()
        view.backgroundColor = .white
        selectedBackgroundView = view
        setSetup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: Setup stack view
    private func setSetup() {
        nameLabel.textColor = .white
        detailInfoTextLabel.textColor = .white
        nameLabel.font = .boldSystemFont(ofSize: Constants.InfoCellSetup.fontSize)
        detailInfoTextLabel.font = .boldSystemFont(ofSize: Constants.InfoCellSetup.detailFontSize)
        stackView = UIStackView(
            arrangedSubviews: [
                nameLabel,
                detailInfoTextLabel
            ]
        )
        colorView.translatesAutoresizingMaskIntoConstraints = false
        stackView.translatesAutoresizingMaskIntoConstraints = false
        colorView.backgroundColor = .red
        contentView.addSubview(colorView)
        contentView.addSubview(stackView)
        viewColorConstraints()
        setConstraints()
        stackView.axis = .vertical
        stackView.spacing = Constants.InfoCellSetup.spacing
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = .init(
            top: Constants.InfoCellSetup.margin,
            left: Constants.InfoCellSetup.margin,
            bottom: Constants.InfoCellSetup.margin,
            right: Constants.InfoCellSetup.margin
        )
        stackView.distribution = .fillEqually
        
    }
    
    //MARK: Add color view constraints
    private func viewColorConstraints() {
        let constraints = [
            colorView.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: Constants.Constraints.top
            ),
            colorView.leadingAnchor.constraint(
                equalTo: contentView.leadingAnchor,
                constant: Constants.Constraints.leading
            ),
            colorView.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: .zero
            ),
            colorView.widthAnchor.constraint(equalToConstant: Constants.InfoCellSetup.colorLineWidth)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    //MARK: Add stack view constraints
    private func setConstraints() {
        let constraints = [
            stackView.topAnchor.constraint(
                equalTo: contentView.topAnchor,
                constant: Constants.Constraints.top
            ),
            stackView.leadingAnchor.constraint(
                equalTo: colorView.trailingAnchor,
                constant: .zero
            ),
            stackView.trailingAnchor.constraint(
                equalTo: contentView.trailingAnchor,
                constant: Constants.Constraints.trailing
            ),
            stackView.bottomAnchor.constraint(
                equalTo: contentView.bottomAnchor,
                constant: .zero
            )
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func setConfig(name: String, data: String) {
        nameLabel.text = name
        detailInfoTextLabel.text = data
    }
    
    func setBackgroundColor(_ color: UIColor, lineColor: UIColor) {
        stackView.backgroundColor = color
        colorView.backgroundColor = lineColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if selected {
            CASpringAnimation.pump(view: self)
        }
    }
}
