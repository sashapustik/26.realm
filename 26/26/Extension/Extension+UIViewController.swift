import Foundation
import UIKit

extension UIViewController {
    func alert(message: String) {
        let alertController = UIAlertController(
            title: Constants.Titles.error,
            message: "\(Constants.field) \(message) \(Constants.isEmpty)",
            preferredStyle: .alert
        )
        let okeyAction = UIAlertAction(
            title: Constants.Titles.okey,
            style: .default, handler: nil
        )
        alertController.view.tintColor = .black
        alertController.addAction(okeyAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func fullConstraints(for view: UIView, to mainView: UIView) {
        let constraints = [
            NSLayoutConstraint(
                item: view,
                attribute: .top,
                relatedBy: .equal,
                toItem: mainView,
                attribute: .top,
                multiplier: Constants.multiplier,
                constant: .zero
            ),
            NSLayoutConstraint(
                item: view,
                attribute: .bottom,
                relatedBy: .equal,
                toItem: mainView,
                attribute: .bottom,
                multiplier: Constants.multiplier,
                constant: .zero
            ),
            NSLayoutConstraint(
                item: view,
                attribute: .leading,
                relatedBy: .equal,
                toItem: mainView,
                attribute: .leading,
                multiplier: Constants.multiplier,
                constant: .zero
            ),
            NSLayoutConstraint(
                item: view,
                attribute: .trailing,
                relatedBy: .equal,
                toItem: mainView,
                attribute: .trailing,
                multiplier: Constants.multiplier,
                constant: .zero
            )
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    func hiddenKeyboard(to tableView: UITableView) {
        NotificationCenter.default.addObserver(
            forName: UIApplication.keyboardWillHideNotification,
            object: nil,
            queue: nil
        ) { _ in
            tableView.contentInset = .zero
        }
        NotificationCenter.default.addObserver(
            forName: UIApplication.keyboardWillShowNotification,
            object: nil,
            queue: nil
        ) { _ in
            let section = tableView.rect(forSection: Constants.Section.editSection)
            var contentInset: UIEdgeInsets = tableView.contentInset
            contentInset.bottom = section.height
            tableView.contentInset = contentInset
        }
    }
}
