import Foundation
import UIKit

extension UITextField {
    func borderColor(_ color: UIColor) {
        self.layer.borderWidth = Constants.borderWidth
        self.layer.borderColor = color.cgColor
    }
}
