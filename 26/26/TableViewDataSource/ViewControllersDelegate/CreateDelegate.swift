import Foundation
import RealmSwift

protocol CreateDelegate: class {
    var object: Object? { get set }
    func set(data: (String, String), type: Object.Type)
    func setInfo(with object: Object) -> [String]
    func setErrorAlert(messsage: String)
    func addButton(isHidden: Bool)
}
