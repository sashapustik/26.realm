import UIKit
import RealmSwift

class CreateProvider: NSObject {
    
    private var textFieldModels: [TextFieldModel]
    private var object: Object?
    private var type: Object.Type
    
    weak var createDelegate: CreateDelegate?
    weak var setInfoDelegate: SetInfoDelegate?
    
    //MARK: Init
    init(textFieldModels: [TextFieldModel], object: Object?, type: Object.Type) {
        self.textFieldModels = textFieldModels
        self.type = type
        if object == nil {
            self.object = type.init()
            self.object?.setValue(Int(arc4random()), forKey: Constants.id)
        } else {
            guard let object = object else { return }
            self.object = object
        }
    }
    
    func getObject() -> Object {
        guard let object = object else { return Object() }
        return object
    }
    
    //MARK: Get and reload data for table
    func reloadData() {
        guard let realm = try? Realm() else { return }
        switch type {
        case is StudentModel.Type:
            guard let object = object as? StudentModel else { return }
            self.object = realm.object(
                ofType: StudentModel.self,
                forPrimaryKey: object.id
            )
        case is CourseModel.Type:
            guard let object = object as? CourseModel else { return }
            self.object = realm.object(
                ofType: CourseModel.self,
                forPrimaryKey: object.id
            )
        case is TeacherModel.Type:
            guard let object = object as? TeacherModel else { return }
            self.object = realm.object(
                ofType: TeacherModel.self,
                forPrimaryKey: object.id
            )
        default:
            break
        }
    }
    
    //MARK: Headers for tableView
    private func getHeaderView() -> [String] {
        var string = String()
        switch type {
        case is StudentModel.Type:
            string = Constants.Titles.courses
        case is CourseModel.Type:
            string = Constants.Titles.students
        case is TeacherModel.Type:
            string = Constants.Titles.courses
        default:
            break
        }
        return [Constants.Titles.edit, string]
    }
}

//MARK: UITableViewDataSource
extension CreateProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sectionCount = Constants.Section.editSectionCount
        switch type {
        case is StudentModel.Type:
            guard let object = object as? StudentModel else { return sectionCount }
            guard section != Constants.Section.editSection else { return sectionCount }
            return object.courses.count
        case is CourseModel.Type:
            guard let object = object as? CourseModel else { return sectionCount }
            if section == Constants.Section.editSection {
                return Constants.Section.editSectionCount
            } else {
                return object.students.count
            }
        case is TeacherModel.Type:
            guard let object = object as? TeacherModel else { return sectionCount }
            guard section != Constants.Section.editSection else { return sectionCount }
            return object.courses.count
        default:
            return Constants.Section.editSectionCount
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let sectionCount = Constants.Section.editSectionCount
        switch type {
        case is StudentModel.Type:
            guard let object = object as? StudentModel else { return sectionCount }
            return object.courses.isEmpty
                ? Constants.Section.editSectionCount
                : Constants.Section.infoCount
        case is CourseModel.Type:
            guard let object = object as? CourseModel else { return sectionCount }
            let strings = Set(object.getAllData())
            return strings.count < 1
                ? Constants.Section.editSectionCount
                : Constants.Section.infoCount
        case is TeacherModel.Type:
            guard let object = object as? TeacherModel else { return sectionCount }
            return object.courses.isEmpty
                ? Constants.Section.editSectionCount
                : Constants.Section.infoCount
        default:
            return Constants.Section.editSectionCount
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard
            let view = tableView.dequeueReusableHeaderFooterView(
                withIdentifier: Constants.CellId.headerId
            ) as? HeaderView
        else { return UIView() }
        view.setConfig(title: getHeaderView()[section])
        view.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7593022561)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.section == Constants.Section.editSection else {
            guard let cell = tableView.dequeueReusableCell(
                withIdentifier: Constants.CellId.itemCell,
                for: indexPath
            ) as? ItemTableViewCell else { return UITableViewCell() }
            cell.setBackgroundColor(#colorLiteral(red: 0.8416554928, green: 0.8417773247, blue: 0.8486333489, alpha: 1), lineColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
            switch type {
            case is StudentModel.Type:
                guard let object = object as? StudentModel else { return UITableViewCell() }
                cell.setConfig(
                    name: object.courses[indexPath.row].name,
                    data: object.courses[indexPath.row].subject
                )
            case is CourseModel.Type:
                guard let object = object as? CourseModel else { return UITableViewCell() }
                let studentIndexPath = indexPath.row
                let name = object.students[studentIndexPath].name
                let surname = object.students[studentIndexPath].surname
                cell.setConfig(
                    name: "\(name) \(surname)",
                    data: object.students[studentIndexPath].mailAddress
                )
                cell.setBackgroundColor(#colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1), lineColor: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1))
            case is TeacherModel.Type:
                guard let object = object as? TeacherModel else { return UITableViewCell() }
                cell.setConfig(
                    name: object.courses[indexPath.row].name,
                    data: object.courses[indexPath.row].subject
                )
            default:
                break
            }
            return cell
        }
        
        guard
            let cell = tableView.dequeueReusableCell(
                withIdentifier: Constants.CellId.createCell,
                for: indexPath
            ) as? CreateTableViewCell
        else { return UITableViewCell() }
        cell.delegate = createDelegate
        guard let object = createDelegate?.object else { return UITableViewCell() }
        cell.setConfig(
            textFieldsModels: textFieldModels,
            to: type,
            data: createDelegate?.setInfo(with: object)
        )
        return cell
    }
}

extension CreateProvider: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var createViewController: CreateViewController?
        
        let tags = Constants.TabBarSetup.Tags.self
        guard indexPath.section == Constants.Section.studentSection else { return }
        switch type {
        case is StudentModel.Type:
            guard let student = object as? StudentModel else { return }
            createViewController = CreateViewController(
                textFieldModels: TextFieldModel.getTextFields(with: tags.course.rawValue),
                object: student.courses[indexPath.row],
                type: CourseModel.self
            )
            
        case is CourseModel.Type:
            guard let course = object as? CourseModel else { return }
            createViewController = CreateViewController(
                textFieldModels: TextFieldModel.getTextFields(with: tags.student.rawValue),
                object: course.students[indexPath.row],
                type: StudentModel.self
            )
        case is TeacherModel.Type :
            guard let teacher = object as? TeacherModel else { return }
            createViewController = CreateViewController(
                textFieldModels: TextFieldModel.getTextFields(with: tags.course.rawValue),
                object: teacher.courses[indexPath.row],
                type: CourseModel.self
            )
        default:
            break
        }
        guard let setViewController = createViewController else { return }
        setInfoDelegate?.setInfo(to: setViewController)
    }
    
    func tableView(
        _ tableView: UITableView,
        editingStyleForRowAt indexPath: IndexPath
    ) -> UITableViewCell.EditingStyle {
        guard indexPath.section == Constants.Section.editSection else { return .delete }
        return .none
    }
    
    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        let itemsSection = indexPath.section == Constants.Section.studentSection
        guard itemsSection else { return }
        let realm = try? Realm()
        try? realm?.write {
            switch type {
            case is StudentModel.Type:
                guard let object = object as? StudentModel else { return }
                let course = object.courses[indexPath.row]
                guard let studentIndex = course.students.firstIndex(of: object) else { return }
                course.students.remove(at: studentIndex)
                object.courses.remove(at: indexPath.row)
            case is CourseModel.Type:
                guard let object = object as? CourseModel else { return }
                let student = object.students[indexPath.row]
                guard let courseIndex = student.courses.firstIndex(of: object) else { return }
                student.courses.remove(at: courseIndex)
                object.students.remove(at: indexPath.row)
            case is TeacherModel.Type:
                guard let object = object as? TeacherModel else { return }
                object.courses[indexPath.row].teacher = nil
                object.courses.remove(at: indexPath.row)
            default:
                break
            }
        }
        tableView.reloadData()
    }
}
