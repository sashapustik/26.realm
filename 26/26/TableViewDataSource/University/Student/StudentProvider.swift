import UIKit
import RealmSwift


class StudentProvider: NSObject,  UniversityProtocol {
    typealias Item = StudentModel
    
    var items: [StudentModel]
    private var course: CourseModel?
    
    weak var sendDelegate: SetInfoDelegate?
    
    override init() {
        self.items = DataBaseService<StudentModel>().get()
    }
    
    func courseIsSelected() -> Bool {
        return course != nil
    }
    
    func setCourse(course: CourseModel) {
        self.course = course
    }
    
    func reloadData() {
        self.items = DataBaseService<StudentModel>().get()
    }
}

//MARK: UITableViewDataSource
extension StudentProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.CellId.defaultCell,
            for: indexPath
        ) as? ItemTableViewCell
        else { return UITableViewCell() }
        
        if course != nil {
            guard
                let isContain = course?.students.contains(items[indexPath.row])
            else { return UITableViewCell() }
            cell.setBackgroundColor(
                isContain
                    ? #colorLiteral(red: 1, green: 0.5040131469, blue: 0.3863841292, alpha: 1)
                    : #colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1),
                lineColor: isContain
                    ? #colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1)
                    : #colorLiteral(red: 1, green: 0.5040131469, blue: 0.3863841292, alpha: 1)
                )
        } else {
            cell.setBackgroundColor(#colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1), lineColor: #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1))
        }
        
        let fullName = "\(items[indexPath.row].name) \(items[indexPath.row].surname)"
        cell.setConfig(
            name: fullName,
            data: items[indexPath.row].mailAddress
        )
      
        return cell
    }
}

//MARK: UITableViewDelegate
extension StudentProvider: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tags = Constants.TabBarSetup.Tags.self
        guard course != nil else {
            let viewController = CreateViewController(
                textFieldModels: TextFieldModel.getTextFields(with: tags.student.rawValue),
                object: items[indexPath.row],
                type: StudentModel.self
            )
            sendDelegate?.setInfo(to: viewController)
            return
        }
        
        guard let isContain = course?.students.contains(items[indexPath.row]) else { return }
        guard isContain else {
            try? Realm().write {
                guard let course = course else { return }
                course.students.append(items[indexPath.row])
                items[indexPath.row].courses.append(course)
            }
            tableView.reloadData()
            return
        }
        
        guard let firstIndex = course?.students.firstIndex(of: items[indexPath.row]) else { return }
        try? Realm().write {
            guard
                let course = course,
                let firstIndexC = items[indexPath.row].courses.firstIndex(
                    of: course
                )
            else { return }
            items[indexPath.row].courses.remove(at: firstIndexC)
            course.students.remove(at: firstIndex)
         }
        tableView.reloadData()
    }
    
    func tableView(
        _ tableView: UITableView,
        editingStyleForRowAt indexPath: IndexPath
    ) -> UITableViewCell.EditingStyle {
        guard course == nil else {
            return .none
        }
        return .delete
    }

    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        let student = items[indexPath.row]
        let realm = try? Realm()
        try? realm?.write {
            realm?.delete(student)
        }
        items.remove(at: indexPath.row)
        tableView.reloadData()
    }
}
