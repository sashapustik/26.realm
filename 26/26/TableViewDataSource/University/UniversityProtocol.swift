import UIKit
import Realm

protocol UniversityProtocol: class, UITableViewDelegate, UITableViewDataSource {
    associatedtype Item
    var items: [Item] { get set }
    func reloadData()
    var sendDelegate: SetInfoDelegate? { get set }
}
