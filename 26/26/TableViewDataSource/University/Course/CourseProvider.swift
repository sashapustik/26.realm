import UIKit
import RealmSwift

class CourseProvider: NSObject, UniversityProtocol {
    typealias Item = CourseModel
    var items: [CourseModel]
    
    private let databaseService = DataBaseService<CourseModel>()
    weak var sendDelegate: SetInfoDelegate?
    
    override init() {
        self.items = databaseService.get()
    }
    
    func reloadData() {
        self.items = databaseService.get()
    }
}

//MARK: UITableViewDataSource
extension CourseProvider: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: Constants.CellId.defaultCell,
            for: indexPath
        ) as? ItemTableViewCell else { return UITableViewCell() }
        
        cell.setConfig(
            name: items[indexPath.row].name,
            data: items[indexPath.row].teacher?.name ?? Constants.Titles.teacherDontSelectTitle
        )
        cell.setBackgroundColor(#colorLiteral(red: 0.8416554928, green: 0.8417772651, blue: 0.8451326489, alpha: 1), lineColor: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1))
        return cell
    }
}

//MARK: UITableViewDelegate
extension CourseProvider: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tags = Constants.TabBarSetup.Tags.self
        let viewController =  CreateViewController(
            textFieldModels: TextFieldModel.getTextFields(with: tags.course.rawValue),
            object: items[indexPath.row],
            type: CourseModel.self
        )
        sendDelegate?.setInfo(to: viewController)
    }
    
    func tableView(
        _ tableView: UITableView,
        commit editingStyle: UITableViewCell.EditingStyle,
        forRowAt indexPath: IndexPath
    ) {
        guard editingStyle == .delete else { return }
        try? databaseService.realm.write {
            databaseService.realm.delete(items[indexPath.row])
            items.remove(at: indexPath.row)
        }
        tableView.reloadData()
    }
}
