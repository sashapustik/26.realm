import Foundation
import UIKit

struct Constants {
    
    static let multiplier: CGFloat = 1
    static let borderWidth: CGFloat = 1

    static let tableViewEdge: CGFloat = 5
    static let cellCorner: CGFloat = 15
    static let id = "id"

    static let field = NSLocalizedString("Field", comment: "")
    static let isEmpty = NSLocalizedString("is empty", comment: "")
    
    struct Section {
        static let numberOfInfoCell = 1
        static let editSectionCount = 1
        static let infoCount = 2
        static let editSection = 0
        static let studentSection = 1
        static let buttonRow = 0
        static let buttonRowCount = 1
    }
    
    struct CellId {
        static let headerId = "header"
        static let defaultCell = "cell"
        static let createCell = "createCell"
        static let itemCell = "itemCell"
    }
    
    struct Constraints {
        
        struct CellLabel {
            static let top: CGFloat = 5
            static let leading: CGFloat = 15
        }
        
        static let editEdge: CGFloat = 5
        static let top: CGFloat = 15
        static let trailing: CGFloat = -15
        static let leading: CGFloat = 15
        static let height: CGFloat = 35
    }
    
    struct TextFieldPlaceholders {
        static let name = NSLocalizedString("Name", comment: "")
        static let surname = NSLocalizedString("Surname", comment: "")
        static let mailAddress = "aaaaaa@aa.com"
        static let subject = NSLocalizedString("Subject", comment: "")
        static let department = NSLocalizedString("Department", comment: "")
        static let teacherName = NSLocalizedString("Teacher name", comment: "")
        static let teacherSurname = NSLocalizedString("Teacher surname", comment: "")
    }
    
    struct Format {
        static let mail = "[A-Z0-9a-z._]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        static let predicateFormat = "SELF MATCHES %@"
        static let onlyString = "[A-Za-z\\sА-Яа-я]{1,64}"
    }
    
    struct Titles {
        static let add = NSLocalizedString("Add", comment: "")
        static let edit = NSLocalizedString("Edit", comment: "")
        static let courses = NSLocalizedString("Courses", comment: "")
        static let students = NSLocalizedString("Students", comment: "")
        static let courseCountTitle = NSLocalizedString("Course count:", comment: "")
        static let teacherTitle = NSLocalizedString("Teacher", comment: "")
        static let teacherDontSelectTitle = NSLocalizedString("Teacher dont select", comment: "")
        static let error = NSLocalizedString("Error", comment: "")
        static let okey = NSLocalizedString("Okey", comment: "")
        static let addTeacher = NSLocalizedString("Add teacher", comment: "")
        static let changeTeacher = NSLocalizedString("Change teacher", comment: "")
        static let fontSize: CGFloat = 20
        static let teacher = NSLocalizedString("Teacher", comment: "")
        static let teacherLabelFontSize: CGFloat = 13
    }
    
    struct Messages {
        static let error = NSLocalizedString("Error", comment: "")
        static let name = NSLocalizedString("Set name to field", comment: "")
        static let surname = NSLocalizedString("Set surname to field", comment: "")
        static let subject = NSLocalizedString("Set subject to field", comment: "")
        static let department = NSLocalizedString("Set department to field", comment: "")
        static let mailAddress = NSLocalizedString("Set mail address to field", comment: "")
        
    }
    
    enum Tags: Int {
        case first = 0
        case second = 1
        case third = 2
    }
    
    struct TabBarSetup {
        struct Images {
            static let student = "studentdesk"
            static let course = "book.fill"
            static let teacher = "person.fill"
        }
        
        struct Names {
            static let student = NSLocalizedString("Student", comment: "")
            static let course = NSLocalizedString("Course", comment: "")
            static let teacher = NSLocalizedString("Teacher", comment: "")
        }
        
        enum Tags: Int {
            case student, course, teacher
        }
    }
    
    struct InfoCellSetup {
        static let spacing: CGFloat = 10
        static let margin: CGFloat = 10
        static let fontSize: CGFloat = 23
        static let detailFontSize: CGFloat = 15
        static let colorLineWidth: CGFloat = 3
    }
    
    
    struct HeaderView {
        static let fontSize: CGFloat = 18
    }
    
    struct Animation {
        static let key = "transform.scale"
        static let fromValue = 0.96
        static let toValue = 1
        static let duration = 0.3
    }
    
    struct ButtonSetup {
        static let cornerRadius: CGFloat = 15
        static let numberOfLines = 1
        static let edgeXY: CGFloat = 10
        static let size: CGFloat = 50
        static let shadowOpacity: Float = 0.3
        static let shodowOffset: CGFloat = 1
    }
}
