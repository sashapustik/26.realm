import Foundation
import RealmSwift

@objcMembers
class CourseModel: Object {
    dynamic var id = Int()
    dynamic var name = String()
    dynamic var subject = String()
    dynamic var department = String()
    dynamic var teacher: TeacherModel? = nil
    var students = List<StudentModel>()
    
    override class func primaryKey() -> String? {
        return Constants.id
    }
    
    func getAllData() -> [String] {
        if let teacherName = teacher?.name, let teacherSurname = teacher?.surname {
            return [name, subject, department, teacherName, teacherSurname]
        } else {
            return [name, subject, department]
        }
    }
}
