import Foundation
import RealmSwift

@objcMembers
class TeacherModel: Object {
    dynamic var id = Int()
    dynamic var name = String()
    dynamic var surname = String()
    var courses = List<CourseModel>()
    
    override class func primaryKey() -> String? {
        return Constants.id
    }
    
    func getAllData() -> [String] {
        return [name, surname]
    }
}
