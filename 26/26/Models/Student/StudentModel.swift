import Foundation
import RealmSwift

@objcMembers
class StudentModel: Object {
    dynamic var id = Int()
    dynamic var name = String()
    dynamic var surname = String()
    dynamic var mailAddress = String()
    var courses = List<CourseModel>()
    
    override class func primaryKey() -> String? {
        return Constants.id
    }
    
    func getAllData() -> [String] {
        return [name, surname, mailAddress]
    }
}


