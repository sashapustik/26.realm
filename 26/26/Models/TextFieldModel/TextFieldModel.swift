import Foundation
import UIKit

struct TextFieldModel {
    var textFieldId: TextFieldId
    var placeholder: String
    var regularExpression: String?
    var textFieldKeyboardStyle: UIKeyboardType
    
    static func getTextFields(with id: Int) -> [TextFieldModel] {
        let tags = Constants.TabBarSetup.Tags.self
        switch id {
        case tags.student.rawValue:
            let textFieldModels = [
                TextFieldModel(
                    textFieldId: .name,
                    placeholder: Constants.TextFieldPlaceholders.name,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                ),
                TextFieldModel(
                    textFieldId: .surname,
                    placeholder: Constants.TextFieldPlaceholders.surname,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                ),
                TextFieldModel(
                    textFieldId: .mailAddress,
                    placeholder: Constants.TextFieldPlaceholders.mailAddress,
                    regularExpression: Constants.Format.mail,
                    textFieldKeyboardStyle: .emailAddress
                )
            ]
            return textFieldModels
        case tags.course.rawValue:
            let textFieldModels = [
                TextFieldModel(
                    textFieldId: .name,
                    placeholder: Constants.TextFieldPlaceholders.name,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                ),
                TextFieldModel(
                    textFieldId: .subject,
                    placeholder: Constants.TextFieldPlaceholders.subject,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                ),
                TextFieldModel(
                    textFieldId: .department,
                    placeholder: Constants.TextFieldPlaceholders.department,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                )
            ]
            return textFieldModels
        case tags.teacher.rawValue:
            let textFieldModels = [
                TextFieldModel(
                    textFieldId: .name,
                    placeholder: Constants.TextFieldPlaceholders.name,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                ),
                TextFieldModel(
                    textFieldId: .surname,
                    placeholder: Constants.TextFieldPlaceholders.surname,
                    regularExpression: Constants.Format.onlyString,
                    textFieldKeyboardStyle: .default
                )
            ]
            return textFieldModels
        default:
            break
        }
        return []
    }
}
